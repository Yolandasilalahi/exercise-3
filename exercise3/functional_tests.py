from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest

class NewVisitorTest(unittest.TestCase):  

    def setUp(self):  
        self.browser = webdriver.Chrome()

    def tearDown(self):  
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_todo_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):  
        
        self.browser.get('http://localhost:8000')
        
        inputbox = self.browser.find_element_by_id('id_new_item')  
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )
        
        #unit test, komentar otomatis jika ada item lebih dari 6 di To-Do list
        self.check_for_row_in_list_table('4. kebanyakan tugas tapi ngga pernal live stream')

        inputbox.send_keys('tugas tapi ngga pernah live stream')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(10)

        #jika semuanya true maka test berhasil dan akan menampilkan pesan di CMD
        self.fail('OK Finish !')
    
if __name__ == '__main__':
    unittest.main(warnings='ignore')